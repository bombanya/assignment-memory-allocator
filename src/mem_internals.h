#ifndef _MEM_INTERNALS_
#define _MEM_INTERNALS_

#include <stdbool.h>
#include <inttypes.h>
#include <stddef.h>

#define REGION_MIN_SIZE (2 * 4096)
#define INITIAL_HEAP_SIZE (REGION_MIN_SIZE)
#define DEBUG_FIRST_BYTES 4

typedef struct { size_t bytes; } block_capacity;
typedef struct { size_t bytes; } block_size;

struct block_header {
    struct block_header* next;
    block_capacity       capacity;
    bool                 is_free;
    uint8_t              contents[];
};

static inline block_size size_from_capacity( block_capacity cap ) {
    return (block_size) {cap.bytes + offsetof( struct block_header, contents ) };
}

static inline block_capacity capacity_from_size( block_size sz ) {
    return (block_capacity) {sz.bytes - offsetof( struct block_header, contents ) };
}

static inline struct block_header* block_get_header(void const* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

#endif
