#ifndef LL_LABA4_MEM_DEBUG_H
#define LL_LABA4_MEM_DEBUG_H

#include <stdio.h>

void debug_struct_info( FILE* f, void const* address );
void debug_heap( FILE* f,  void const* ptr );

#endif //LL_LABA4_MEM_DEBUG_H
