#include "mem_tests.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void run_mem_tests(){
    printf("running allocator tests\n\n");

    test_simple_alloc();
    test_simple_free();
    test_free_with_merge();
    test_min_capacity();
    test_allocate_new_region();

    printf("all tests passed\n");
}

void test_simple_alloc(){
    printf("allocating 40 and 8 bytes blocks\nsecond block should have 24 bytes capacity\n");
    void* a = custom_malloc(40);
    void* b = custom_malloc(8);
    debug_mem(stdout);
    struct block_header* block_a = block_get_header(a);
    struct block_header* block_b = block_get_header(b);
    if (!block_a->is_free && block_a->capacity.bytes == 40
            && !block_b->is_free && block_b->capacity.bytes == 24){
        printf("test passed\n\n");
        custom_free(a);
        custom_free(b);
    }
    else err("test failed\n");
}

void test_simple_free(){
    printf("allocating 40, 8 and 64 bytes blocks,\nthen freeing second one\n");
    void* a = custom_malloc(40);
    void* b = custom_malloc(8);
    void* c = custom_malloc(64);
    custom_free(b);
    debug_mem(stdout);
    struct block_header* block_a = block_get_header(a);
    struct block_header* block_b = block_get_header(b);
    struct block_header* block_c = block_get_header(c);
    if (!block_a->is_free && block_a->capacity.bytes == 40
            && block_b->is_free && block_b->capacity.bytes == 24
            && !block_c->is_free && block_c->capacity.bytes == 64){
        printf("test passed\n\n");
        custom_free(a);
        custom_free(c);
    }
    else err("test failed\n");
}

void test_free_with_merge(){
    printf("allocating 40, 8 and 64 bytes blocks,\nthen freeing first and second\n");
    void* a = custom_malloc(40);
    void* b = custom_malloc(8);
    void* c = custom_malloc(64);
    debug_mem(stdout);
    custom_free(b);
    custom_free(a);
    debug_mem(stdout);
    struct block_header* block_a = block_get_header(a);
    struct block_header* block_c = block_get_header(c);
    if (block_a->is_free && block_a->capacity.bytes == 81
        && !block_c->is_free && block_c->capacity.bytes == 64){
        printf("test passed\n\n");
        custom_free(c);
    }
    else err("test failed\n");
}

void test_min_capacity(){
    printf("create 100 bytes empty block,\nthen allocate 80 bytes checking\nthat it won't split the block\n");
    void* a = custom_malloc(100);
    void* b = custom_malloc(64);
    debug_mem(stdout);
    custom_free(a);
    a = custom_malloc(80);
    debug_mem(stdout);
    struct block_header* block_a = block_get_header(a);
    struct block_header* block_b = block_get_header(b);
    if (!block_a->is_free && block_a->capacity.bytes == 100
        && !block_b->is_free && block_b->capacity.bytes == 64){
        printf("test passed\n\n");
        custom_free(a);
        custom_free(b);
    }
    else err("test failed\n");
}

void test_allocate_new_region(){
    printf("allocate 8000 bytes and then 1000 bytes\nforcing to allocate a new region\n");
    void* a = custom_malloc(8000);
    void* b = custom_malloc(1000);
    debug_mem(stdout);
    struct block_header* block_a = block_get_header(a);
    struct block_header* block_b = block_get_header(b);
    if (!block_a->is_free && block_a->capacity.bytes == 8000
        && !block_b->is_free && block_b->capacity.bytes == 1000){
        printf("test passed\n\n");
        custom_free(a);
        custom_free(b);
    }
    else err("test failed\n");
}
