#include "mem.h"
#include <unistd.h>
#include <sys/mman.h>
#include "mem_internals.h"
#include "mem_debug.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

#ifndef HEAP_START
#define HEAP_START ((void*)0)
#endif

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};

struct allocation_result {
    enum {ALLOC_FIXED_SUCCESS, ALLOC_SUCCESS, ALLOC_FAIL} type;
    struct block_header* block;
};

static void* heap_start_addr = (void*)-1;

static bool block_is_big_enough( struct block_header* block, size_t query) {
    return block->capacity.bytes >= query;
}

static size_t pages_count( size_t mem ){
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages( size_t mem ){
    return getpagesize() * pages_count( mem ) ;
}

static size_t region_actual_size( size_t query ) {
    return size_max( round_pages( query ), REGION_MIN_SIZE );
}

static void block_init( void* restrict addr, block_capacity block_cp, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = block_cp,
    .is_free = true
    };
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE,
                 MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

static void* block_after( struct block_header const* block ) {
    return  (void*) (block->contents + block->capacity.bytes);
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
    return block-> is_free && query +
    offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static void split_if_too_big( struct block_header* block, size_t query ) {
    if (block_splittable(block, query)){
        size_t old_cp = block->capacity.bytes;
        block->capacity.bytes = query;
        block_init(block_after(block),
                   (block_capacity) {old_cp - query - offsetof(struct block_header, contents)},
                           block->next);
        block->next = block_after(block);
    }
}


/*  --- Слияние соседних свободных блоков --- */
static bool blocks_continuous (struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static void try_merge_with_next( struct block_header* block ) {
    while (block->next != NULL && mergeable(block, block->next)){
        block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
        block->next = block->next->next;
    }
}

static struct allocation_result try_fixed_allocation(struct block_header* restrict last, size_t query){
    if (last->is_free) query -= last->capacity.bytes;
    else query += offsetof(struct block_header, contents);
    block_size size = (block_size) {region_actual_size(query)};
    if (map_pages(block_after(last),size.bytes,MAP_FIXED_NOREPLACE) != MAP_FAILED){
        if (last->is_free) {
            last->capacity.bytes += size.bytes;
            return (struct allocation_result) {.type = ALLOC_FIXED_SUCCESS, .block = last};
        }
        else{
            block_init(block_after(last), capacity_from_size(size), NULL);
            last->next = block_after(last);
            return (struct allocation_result) {.type = ALLOC_FIXED_SUCCESS, .block = block_after(last)};
        }
    }
    return (struct allocation_result) {.type = ALLOC_FAIL};
}

static struct allocation_result allocate_somewhere(struct block_header* restrict last, size_t query){
    void* mapping;
    block_size size = (block_size) {region_actual_size(query + offsetof(struct block_header, contents))};
    if ((mapping = map_pages(NULL, size.bytes,0)) != MAP_FAILED){
        block_init(mapping, capacity_from_size(size), NULL);
        last->next = mapping;
        return (struct allocation_result) {.type = ALLOC_SUCCESS, .block = mapping};
    }
    else return (struct allocation_result) {.type = ALLOC_FAIL};
}

//в случае успеха вернет структуру с указателем на последний блок кучи
static struct allocation_result grow_heap(struct block_header* restrict last, size_t query ) {
    struct allocation_result res = try_fixed_allocation(last, query);
    if (res.type == ALLOC_FIXED_SUCCESS) return res;
    return allocate_somewhere(last, query);
}

static struct block_search_result find_good_or_last ( struct block_header* block, size_t sz ){
     for (;;){
         if (block->is_free) try_merge_with_next(block);
         if (block->is_free && block_is_big_enough(block, sz)){
             return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
         }
         if (!block->next) break;
         block = block->next;
    }
    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( struct block_header* block, size_t query ) {
    struct block_search_result search_result = find_good_or_last(block, query);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(search_result.block, query);
        search_result.block->is_free = false;
    }
    return search_result;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(  struct block_header* heap_start, size_t query) {
    struct block_search_result search_result = try_memalloc_existing(heap_start, query);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) return search_result.block;
    if (search_result.type == BSR_CORRUPTED) return NULL;
    struct allocation_result al_res = grow_heap(search_result.block, query);
    if (al_res.type == ALLOC_FAIL) return NULL;
    search_result = try_memalloc_existing(al_res.block, query);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) return search_result.block;
    else return NULL;
}

static void heap_init() {
    heap_start_addr = map_pages(HEAP_START, INITIAL_HEAP_SIZE, 0);
    if (heap_start_addr == MAP_FAILED) err("heap cannot be initialized");
    block_init(heap_start_addr,
               capacity_from_size((block_size) {INITIAL_HEAP_SIZE}), NULL);
}

void* custom_malloc( size_t query ) {
    if (heap_start_addr == (void*)-1) heap_init();
    if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
    struct block_header* const addr = memalloc((struct block_header*) heap_start_addr, query);
    if (addr) return addr->contents;
    else return NULL;
}

void custom_free( void* mem ) {
    if (!mem) return;
    struct block_header* header = block_get_header( mem );
    header->is_free = true;
    try_merge_with_next(header);
}

void debug_mem(FILE* f){
    debug_heap(f, heap_start_addr);
}

void debug_block(FILE* f, void const* addr){
    debug_struct_info(f, block_get_header(addr));
}
