#ifndef _MEM_H_
#define _MEM_H_

#include <stdio.h>

void* custom_malloc(size_t query );
void  custom_free( void* mem );

void debug_mem(FILE* f);
void debug_block(FILE* f, void const* addr);

#endif
