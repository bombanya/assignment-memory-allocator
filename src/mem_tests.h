#ifndef LL_LABA4_MEM_TESTS_H
#define LL_LABA4_MEM_TESTS_H

void run_mem_tests();
void test_simple_alloc();
void test_simple_free();
void test_free_with_merge();
void test_min_capacity();
void test_allocate_new_region();

#endif //LL_LABA4_MEM_TESTS_H
