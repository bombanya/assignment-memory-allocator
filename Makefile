CFLAGS=--std=gnu17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror
BUILDDIR=build
SRCDIR=src
CC=gcc


all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/main.o $(BUILDDIR)/mem_tests.o
	$(CC) -o $(BUILDDIR)/main $^

fixed_heap: CFLAGS += -DHEAP_START='((void*)0x04040000)'
fixed_heap: all

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/%.o: $(SRCDIR)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
